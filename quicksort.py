from typing import Sequence


def quicksort(array: Sequence) -> list:
    if len(array := list(array)) < 2:
        return array

    pivot = len(array) - 1
    left, right = 0, max(1, pivot - 1)

    while True:
        # Position:
        # - left cursor at first number from left greater than pivot
        # - right cursor at first number from right less than pivot
        # then either:
        # - swap elements at left and right index
        # - put pivot in the middle and exit

        while array[left] < array[pivot]:
            left += 1
        while array[right] > array[pivot]:
            right -= 1

        if right <= left:
            array[left], array[pivot] = array[pivot], array[left]
            break
        else:
            array[left], array[right] = array[right], array[left]

    return quicksort(array[:left]) + [array[left]] + quicksort(array[left + 1 :])


if "__main__" == __name__:
    problem = (20, 10, 80, 60, 50, 70, 30, 40)
    result = quicksort(problem)
    assert result == sorted(problem), "Wrong sort output"
    print(f"{result=}")
